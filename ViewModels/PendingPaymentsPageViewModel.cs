﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using CityCableCRM.Common.Constants;
using CityCableCRM.Helpers;
using CityCableCRM.Model;
using Newtonsoft.Json;
using Prism.Navigation;

namespace CityCableCRM.ViewModels
{
    public class PendingPaymentsPageViewModel : BindableBase
    {
        #region Data Members
        private readonly INavigationService _navigationService;
        #endregion

        #region Constructor
        public PendingPaymentsPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            _pendingBillsModel = new PendingBillsModel();
            OnPagePrepration();
            ItemsList = new List<string>
            {
                DateTime.Now.AddYears(-1).Year.ToString(),
                DateTime.Now.Year.ToString(),
                DateTime.Now.AddYears(1).Year.ToString()
            };
            SubmitYear = new DelegateCommand(OnSubmitButtonClickedAsync);
        }
        #endregion

        #region Properties

        private List<string> _itemsList;
        public List<string> ItemsList
        {
            get { return _itemsList; }
            set { _itemsList = value; OnPropertyChanged("ItemsList"); }
        }

        private ObservableCollection<ShowPendingBills> _pendingBills = new ObservableCollection<ShowPendingBills>();
        public ObservableCollection<ShowPendingBills> PendingBills
        {
            get { return _pendingBills; }
            set { _pendingBills = value; OnPropertyChanged("PendingBills"); }
        }

        private PendingBillsModel _pendingBillsModel;
        public PendingBillsModel PendingBillsModel
        {
            get { return _pendingBillsModel; }
            set { _pendingBillsModel = value; OnPropertyChanged(); }
        }

        private object _itemSelectedFromList;
        public object ItemSelectedFromList
        {
            get { return _itemSelectedFromList; }
            set { _itemSelectedFromList = value; OnPropertyChanged("ItemSelectedFromList"); }
        }

        private string statusMessage;
        public string StatusMessage
        {
            get { return statusMessage; }
            set { statusMessage = value; OnPropertyChanged(); }
        }

        private bool isStatusVisible;
        public bool IsStatusVisible
        {
            get { return isStatusVisible; }
            set { isStatusVisible = value; OnPropertyChanged(); }
        }
        #endregion

        #region DelegateCommand
        public DelegateCommand SubmitYear { get; set; }
        #endregion

        #region Methods
        private async void OnPagePrepration()
        {
            if (ItemSelectedFromList == null)
                ItemSelectedFromList = DateTime.Now.Year.ToString();

            PendingBills = await GetPendingBills(ItemSelectedFromList.ToString());
        }
        private async Task<ObservableCollection<ShowPendingBills>> GetPendingBills(string year)
        {
            UserDialogs.Instance.Loading("please wait..");
            IsStatusVisible = false;
            var bills = new ObservableCollection<ShowPendingBills>();
            HttpClientHelper apicall = new HttpClientHelper("http://" + Settings.CCC_OperatorUrl + ApiUrls.Url_PaidBill, string.Empty);
            PendingBillsModel.customerid = Settings.CCC_CustomerId;
            PendingBillsModel.year = year;
            var paidbillsjson = JsonConvert.SerializeObject(PendingBillsModel);
            var response = await apicall.Post<PendingBillsResponse>(paidbillsjson);
            if (response?.status != null && response.status == "true")
            {
                for (int count = 0; count < response.data.Count(); count++)
                {
                    bills.Add(new ShowPendingBills()
                    {
                        Month = response.data[count].month,
                        BillNumber = response.data[count].billnumber,
                        PendingAmount = response.data[count].pendingamount
                    });
                }
                if (bills.Count == 0)
                {
                    IsStatusVisible = true;
                    StatusMessage = response.message;
                    UserDialogs.Instance.Loading().Hide();
                }
                DisposeObject();
                UserDialogs.Instance.Loading().Hide();
            }
            else
            {
                IsStatusVisible = true;
                StatusMessage = response.message;
                UserDialogs.Instance.Loading().Hide();
            }
            return bills;
        }
        private void DisposeObject()
        {
            PendingBillsModel = new PendingBillsModel();
        }

        private async void OnSubmitButtonClickedAsync()
        {
            await GetPendingBills(ItemSelectedFromList.ToString());
        }
        #endregion
    }
}
