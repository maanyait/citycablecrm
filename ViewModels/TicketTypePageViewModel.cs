﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using CityCableCRM.Common.Constants;
using CityCableCRM.Helpers;
using CityCableCRM.Model;
using Newtonsoft.Json;
using Prism.Navigation;

namespace CityCableCRM.ViewModels
{
    public class TicketTypePageViewModel : BaseViewModel
    {
        #region Data Members
        private readonly INavigationService _navigationService;
        #endregion

        #region Constructor
        public TicketTypePageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            TicketTypeModel = new TicketTypeModel();
            OnPagePrepration();
            SubmitTicket = new DelegateCommand<ShowTicketTypes>(OnSubmitClickedAsync);
        }
        #endregion

        #region Properties
        private ObservableCollection<ShowTicketTypes> _ticketTypeItems = new ObservableCollection<ShowTicketTypes>();
        public ObservableCollection<ShowTicketTypes> TicketTypeItems
        {
            get { return _ticketTypeItems; }
            set { _ticketTypeItems = value; OnPropertyChanged("TicketTypeItems"); }
        }

        private TicketTypeModel _ticketTypeModel;
        public TicketTypeModel TicketTypeModel
        {
            get { return _ticketTypeModel; }
            set { _ticketTypeModel = value; OnPropertyChanged(); }
        }

        private string statusMessage;
        public string StatusMessage
        {
            get { return statusMessage; }
            set { statusMessage = value; OnPropertyChanged(); }
        }

        private bool isStatusVisible;
        public bool IsStatusVisible
        {
            get { return isStatusVisible; }
            set { isStatusVisible = value; OnPropertyChanged(); }
        }
        #endregion

        #region DelegateCommand
        public DelegateCommand<ShowTicketTypes> SubmitTicket { get; set; }
        #endregion

        #region Methods
        private async void OnPagePrepration()
        {
            TicketTypeItems = await GetTicketTypeList();
        }
        private async Task<ObservableCollection<ShowTicketTypes>> GetTicketTypeList()
        {
            IsStatusVisible = false;
            var tickets = new ObservableCollection<ShowTicketTypes>();
            HttpClientHelper apicall = new HttpClientHelper("http://" + Settings.CCC_OperatorUrl + ApiUrls.Url_TicketType, string.Empty);
            var ticketjson = JsonConvert.SerializeObject(TicketTypeModel);
            var response = await apicall.Post<TicketTypeResponse>(ticketjson);
            if (response?.status != null && response.status == "true")
            {
                foreach (var ticket in response.data)
                {
                    tickets.Add(new ShowTicketTypes()
                    {
                        TicketId = ticket.ticket_id,
                        TicketType = ticket.ticket_type
                    });
                }
                DisposeObject();
            }
            else
            {
                IsStatusVisible = true;
                StatusMessage = response.message;
            }
            return tickets;
        }
        private void DisposeObject()
        {
            TicketTypeModel = new TicketTypeModel();
        }
        private void OnSubmitClickedAsync(ShowTicketTypes showTicketTypes)
        {
            string id = showTicketTypes.TicketId;
            NavigationParameters navigationParameters = new NavigationParameters();
            navigationParameters.Add("ticket_id", id);
            _navigationService.NavigateAsync("OpenTicketPage", navigationParameters, null, false);
        }
        #endregion
    }
}
