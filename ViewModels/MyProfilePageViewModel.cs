﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using CityCableCRM.Helpers;
using Prism.Navigation;

namespace CityCableCRM.ViewModels
{
    public class MyProfilePageViewModel : BindableBase
    {
        private readonly INavigationService _navigationService;

        public MyProfilePageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            ProfilePicImageSource = "http://" + Settings.CCC_ProfilePictureUrl;
            CustomerId = "CustomerId: " + Settings.CCC_CustomerId;
            CustomerName = "Name: " + Settings.CCC_CustomerName;
            CustomerLocation = "Location: " + Settings.CCC_CustomerLocation;
            CustomerAddress = "Address: " + Settings.CCC_CustomerAddress;
            CustomerMobile = "Contact Number: " + Settings.CCC_CustomerMobile;
            CustomerAccountNumber = "Account Number: " + Settings.CCC_CustomerAccountNumber;
        }

        #region Properties

        private string _profilePicImageSource;
        public string ProfilePicImageSource
        {
            get { return _profilePicImageSource; }
            set { _profilePicImageSource = value; OnPropertyChanged("ProfilePicImageSource"); }
        }

        private string _customerId;
        public string CustomerId
        {
            get { return _customerId; }
            set { _customerId = value; OnPropertyChanged("CustomerId"); }
        }

        private string _customerName;
        public string CustomerName
        {
            get { return _customerName; }
            set { _customerName = value; OnPropertyChanged("CustomerName"); }
        }

        private string _customerLocation;
        public string CustomerLocation
        {
            get { return _customerLocation; }
            set { _customerLocation = value; OnPropertyChanged("CustomerLocation"); }
        }

        private string _customerAddress;
        public string CustomerAddress
        {
            get { return _customerAddress; }
            set { _customerAddress = value; OnPropertyChanged("CustomerAddress"); }
        }

        private string _customerMobile;
        public string CustomerMobile
        {
            get { return _customerMobile; }
            set { _customerMobile = value; OnPropertyChanged("CustomerMobile"); }
        }

        private string _customerAccountNumber;
        public string CustomerAccountNumber
        {
            get { return _customerAccountNumber; }
            set { _customerAccountNumber = value; OnPropertyChanged("CustomerAccountNumber"); }
        }
        #endregion
    }
}
