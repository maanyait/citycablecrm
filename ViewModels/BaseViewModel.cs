﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace CityCableCRM.ViewModels
{
    public class BaseViewModel : BindableBase, INotifyPropertyChanged
    {
        public BaseViewModel()
        {

        }

        private bool isRunningTasks;
        /// <summary>
        /// /Get or set the IsRunningTasks
        /// </summary>
        public bool IsRunningTasks
        {
            get { return isRunningTasks; }
            set { isRunningTasks = value; OnPropertyChanged(); }
        }

        private bool isVisible;
        /// <summary>
        /// /Get or set the IsRunningTasks
        /// </summary>
        public bool IsVisible
        {
            get { return isVisible; }
            set { isVisible = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
