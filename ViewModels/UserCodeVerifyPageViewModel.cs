﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using CityCableCRM.Common.Constants;
using CityCableCRM.Helpers;
using CityCableCRM.Model;
using Newtonsoft.Json;
using Prism.Navigation;
using Xamarin.Forms;

namespace CityCableCRM.ViewModels
{
    public class UserCodeVerifyPageViewModel : BaseViewModel
    {
        #region Data Members
        private readonly INavigationService _navigationService;
        #endregion
        public UserCodeVerifyPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            VerifyCommand = new DelegateCommand(OnVerifyButtonClickedAsync);
            UserCodeVerifyModel = new UserCodeVerifyModel();
        }

        #region DelegateCommand
        public DelegateCommand VerifyCommand { get; set; }
        #endregion

        #region Properties
        private UserCodeVerifyModel _userCodeVerifyModel;
        public UserCodeVerifyModel UserCodeVerifyModel
        {
            get { return _userCodeVerifyModel; }
            set { _userCodeVerifyModel = value; OnPropertyChanged(); }
        }
        #endregion

        #region Methods

        private async void OnVerifyButtonClickedAsync()
        {
            if (string.IsNullOrWhiteSpace(UserCodeVerifyModel.OperatorCode))
            {
                await Application.Current.MainPage.DisplayAlert("Verify", "Please enter verification code", "OK");
                return;
            }
            if (!IsRunningTasks)
            {
                IsRunningTasks = true;
                HttpClientHelper apicall = new HttpClientHelper(ApiUrls.Url_UserVerify, string.Empty);
                var verificationjson = JsonConvert.SerializeObject(UserCodeVerifyModel);
                var response = await apicall.Post<CodeVerifyResponse>(verificationjson);
                if (response != null)
                {
                    if (response.status != null && response.status == "true")
                    {
                        Settings.CCC_OperatorCode = response.data.operatorcode;
                        Settings.CCC_OperatorUrl = response.data.operatorurl;
                        await _navigationService.NavigateAsync("LoginPage", null, null, false);
                        DisposeObject();
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Verify", AppConstant.VERIFICATION_FAILURE, "OK");
                    }
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Verify", AppConstant.VERIFICATION_FAILURE, "OK");
                }
            }
            IsRunningTasks = false;
        }

        private void DisposeObject()
        {
            UserCodeVerifyModel = new UserCodeVerifyModel();
        }
        #endregion
    }
}
