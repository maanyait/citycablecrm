﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Prism.Navigation;

namespace CityCableCRM.ViewModels
{
    public class MainMasterDetailPageViewModel : BaseViewModel, INavigationAware
    {
        private readonly INavigationService _navigationService;
        public MainMasterDetailPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {

        }
    }
}
