﻿using CityCableCRM.Helpers;
using CityCableCRM.Views;
using Prism.Unity;

namespace CityCableCRM
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized()
        {
            AppInitializeStep1();
        }

        protected override void RegisterTypes()
        {
            Container.RegisterTypeForNavigation<UserCodeVerifyPage>();
            Container.RegisterTypeForNavigation<LoginPage>();
            Container.RegisterTypeForNavigation<DashboardPage>();
            Container.RegisterTypeForNavigation<MainMasterDetailPage>();
            Container.RegisterTypeForNavigation<CommonNavigationPage>();
            Container.RegisterTypeForNavigation<MenuPage>();
            Container.RegisterTypeForNavigation<GetMyTicketDetailPage>();
            Container.RegisterTypeForNavigation<PaidBillsPage>();
            Container.RegisterTypeForNavigation<MyProfilePage>();
            Container.RegisterTypeForNavigation<PendingPaymentsPage>();
            Container.RegisterTypeForNavigation<OpenTicketPage>();
        }
        private void AppInitializeStep1()
        {
            InitializeComponent();
            if (string.IsNullOrEmpty(Settings.CCC_OperatorCode))
            {
                NavigationService.NavigateAsync("UserCodeVerifyPage");
            }
            else
            {
                NavigationService.NavigateAsync("MainMasterDetailPage/CommonNavigationPage/DashboardPage", null, null, false);
            }
        }

    }
}
