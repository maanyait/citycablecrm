﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityCableCRM.Common.Enumerators
{
    public enum ApplicationActivity
    {
        GetMyTicketDetailPage = 1,
        PaidBillsPage = 2,
        MyProfilePage = 3,
        PendingPaymentsPage = 4
    }
}
