﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityCableCRM.Model
{
    public class CreateOpenTicketModel : BaseModel
    {
        public string tickettypeid { get; set; }
        public string reporteduserid { get; set; }
        public string status { get; set; }
        public string addedby { get; set; }
        public string updatedby { get; set; }
    }
    public class CreateOpenTicketResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public string data { get; set; }
    }
}
