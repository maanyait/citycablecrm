﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityCableCRM.Model
{
    public class GetMyTicketDetailModel : BaseModel
    {
        public string rolename { get; set; }
        public string reporteduserid { get; set; }
        public string assigneduserid { get; set; }
        public string status { get; set; }
        public string pageindex { get; set; }
        public string pagesize { get; set; }
        public string recordcount { get; set; }
    }

    public class GetMyTicketDetailResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public GetMyTicketDetail[] data { get; set; }
    }

    public class GetMyTicketDetail
    {
        public string mytickets_id { get; set; }
        public string mytickets_ticketnumber { get; set; }
        public string mytickets_name { get; set; }
        public string mytickets_location { get; set; }
        public string mytickets_streetname { get; set; }
        public string mytickets_postaladdress { get; set; }
        public string mytickets_status { get; set; }
        public string mytickets_isuuetype { get; set; }
        public string mytickets_assignedusername { get; set; }

    }

    public class ShowMyTicketDetail
    {
        public string TicketIssue { get; set; }
        public string StatusAssignedTo { get; set; }
    }
}
