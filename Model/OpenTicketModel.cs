﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityCableCRM.Model
{
    public class OpenTicketModel : BaseModel
    {

    }

    public class OpenTicketResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public OpenTicketData[] data { get; set; }
    }

    public class OpenTicketData
    {
        public string ticket_id { get; set; }
        public string ticket_type { get; set; }
    }

    public class ShowOpenTicket
    {
        public string TicketId { get; set; }
        public string TicketType { get; set; }
    }
}
